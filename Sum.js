class Sum {
    // let a;
    // let b;
    constructor(a, b) {
        this.a = a;
        this.b = b;
    }

    operate() {
        return this.a + this.b;
    }
}

module.exports = Sum;