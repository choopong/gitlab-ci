const Sum = require('../Sum');

test('adds 1 + 2 to equal 3', () => {
    let sum = new Sum(1, 2);
  expect(sum.operate()).toBe(3);
});